import { Model, DataTypes } from "../deps.ts";
import db from "../config/database.ts";

class User extends Model {
  static table = "users";

  static timestamps = true;

  static fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
    },
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.INTEGER },
  };
}

db.link([User]);

export default User;
