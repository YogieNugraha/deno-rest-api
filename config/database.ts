import { Database } from "../deps.ts";

const db = new Database("postgres", {
  host: "localhost",
  username: "postgres",
  password: "postgres",
  database: "deno-drash",
  port: 5432,
});

export default db;
