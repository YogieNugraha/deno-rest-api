export interface ValidationData {
  success: boolean;
  message: object;
}
