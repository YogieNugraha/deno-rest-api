import { Drash } from "./deps.ts";
import UserResource from "./resources/user_resource.ts";
import db from "./config/database.ts";

await db.sync({ drop: false });

console.log("Database Connected");

const server = new Drash.Http.Server({
  directory: Deno.realPathSync("./"),
  response_output: "application/json",
  logger: new Drash.CoreLoggers.ConsoleLogger({
    enabled: false,
    level: "all",
    tag_string: "{datetime} | {level} |",
    tag_string_fns: {
      datetime() {
        return new Date().toISOString().replace("T", " ");
      },
    },
  }),
  resources: [
    UserResource,
  ],
});

await server.run({
  hostname: "localhost",
  port: 3000,
});

console.log(`Server listening: http://${server.hostname}:${server.port}`);
