import { Drash } from "../deps.ts";
import User from "../model/user_model.ts";
import { validate, required, isNumber, isString } from "../deps.ts";
import { ValidationData } from "../config.ts";

export default class UserResource extends Drash.Http.Resource {
  static paths = ["/api/v1/users", "/api/v1/users/:id"];

  protected async ValidateData(
    method: string,
    inputs: object,
  ): Promise<ValidationData> {
    let passes;
    let errors;

    if (method == "create") {
      [passes, errors] = await validate(inputs, {
        name: [required, isString],
        age: [required, isNumber],
      });
    } else {
      [passes, errors] = await validate(inputs, {
        name: [isString],
        age: [isNumber],
      });
    }

    if (passes != true) {
      this.response.body = {
        success: false,
        message: errors,
      };
      return { success: false, message: errors };
    } else {
      return { success: true, message: {} };
    }
  }

  public async GET() {
    let data;
    
    if (this.request.getPathParam("id")) {
      data = await User.find(this.request.getPathParam("id"));
    } else {
      data = await User.orderBy('id').all();
    }

    if (data === undefined) {
      this.response.status_code = 204;
    } else {
      this.response.body = {
        success: true,
        message: "Success Get Data",
        data: data,
      };
    }
    return this.response;
  }

  public async POST() {
    const inputs = {
      name: this.request.getBodyParam("name"),
      age: this.request.getBodyParam("age"),
    };

    const validation = await this.ValidateData("create", inputs);

    if (validation.success == false) {
      this.response.body = {
        success: false,
        message: validation.message,
      };
      return this.response;
    }

    try {
      await User.create(inputs);

      this.response.body = { success: true, message: "Success Insert Data" };
    } catch (error) {
      this.response.body = { success: true, message: error };
    }
    return this.response;
  }

  public async PUT() {
    const inputs = {
      name: this.request.getBodyParam("name"),
      age: this.request.getBodyParam("age"),
    };

    const validation = await this.ValidateData("update", inputs);

    if (validation.success == false) {
      this.response.body = {
        success: false,
        message: validation.message,
      };
      return this.response;
    }

    try {
      await User.where("id", this.request.getPathParam("id"))
        .update(inputs);
      
      this.response.body = { success: true, message: "Success Update Data" };
    } catch (error) {
      this.response.body = { success: true, message: error };
    }
    return this.response;
  }

  public async DELETE() {
    try {
      await User.deleteById(this.request.getPathParam("id"));

      this.response.body = { success: true, message: "Success Delete Data" };
    } catch (error) {
      this.response.body = { success: true, message: error };
    }
    return this.response;
  }
}
